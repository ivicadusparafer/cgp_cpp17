#include "MultipleClassEvalOp.h"
#include "./cartesian/Cartesian.h"
#include "utility/fileparser.h"
#include "./utility/measures.h"
#include "./utility/utility.h"
#include <boost/algorithm/string.hpp>

void MultipleClassEvalOp::registerParameters(StateP stateP)
{
    stateP->getRegistry()->registerEntry("training.infile", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("testing.infile", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("measure", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("hasID", (voidP) new (std::string), ECF::STRING);
}

bool MultipleClassEvalOp::initialize(StateP stateP)
{
    if(!stateP->getRegistry()->isModified("training.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for training");
        return false;
    }
    if(!stateP->getRegistry()->isModified("testing.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for testing");
        return false;
    }
    if(!stateP->getRegistry()->isModified("measure")) {
        ECF_LOG(stateP,0,"Default measure of accuracy is used.");
        measureUsed = "accuracy";
    }
    else {
        voidP vpM = stateP->getRegistry()->getEntry("measure");
        measureUsed = *((std::string*)vpM.get());
        boost::algorithm::to_lower(measureUsed);
    }
    if(!stateP->getRegistry()->isModified("hasID")) {
        leadsWithId = true;
    }
    //Load training from file.
    voidP vp1 = stateP->getRegistry()->getEntry("training.infile");
    std::string path1 = *((std::string*)vp1.get());
    auto parsedTrainingCSV = utility::parseCSVFromFile(path1);
    auto ioTrainingPair = utility::parseStringIntoFeaturesAndLabels(parsedTrainingCSV, leadsWithId);
    trainingInput = std::move(ioTrainingPair.first);
    trainingOutputs = std::move(ioTrainingPair.second);

    //Load testing from file.
    voidP vp2 = stateP->getRegistry()->getEntry("testing.infile");
    std::string path2 = *((std::string*)vp2.get());
    auto parsedTestCSV = utility::parseCSVFromFile(path2);
    auto ioTestPair = utility::parseStringIntoFeaturesAndLabels(parsedTestCSV, leadsWithId);
    testingInput = std::move(ioTestPair.first);
    testingOutputs = std::move(ioTestPair.second);

    std::set<uint> distinct;
    for(const auto& elem : trainingOutputs) {
        distinct.insert(elem);
    }
    numberOfDifferentClasses = distinct.size();

    if(stateP->getRegistry()->isModified("softtarget")) {
        voidP vpS = stateP->getRegistry()->getEntry("softtarget");
        std::string sa = *((std::string*)vpS.get());
        if(sa == "1" || sa == "yes" || sa == "Yes" || sa == "true") {
            softTarget = true;
            ECF_LOG(stateP,0,"Soft target regularization is used.");
            if(!stateP->getRegistry()->isModified("softtarget.beta")) {
                ECF_LOG_ERROR(stateP,"Could not find beta factor for softtarget. Define softtarget.beta entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSBeta = stateP->getRegistry()->getEntry("softtarget.beta");
                std::string vbeta = *((std::string*)vpSBeta.get());
                try{
                    softTargetBeta = std::stod(vbeta);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP,"Soft target beta is not a number convertible to double.\n");
                    exit(-1);
                }
            }
            if(!stateP->getRegistry()->isModified("softtarget.gamma")) {
                ECF_LOG_ERROR(stateP,"Could not find gamma factor for softtarget. Define softtarget.gamma entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSGamma = stateP->getRegistry()->getEntry("softtarget.gamma");
                std::string vgamma = *((std::string*)vpSGamma.get());
                try{
                    softTargetGamma = std::stod(vgamma);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP, "Soft target gamma is not a number convertible to double.\n");
                    exit(-1);
                }
            }
        }
    }
    return true;
}

FitnessP MultipleClassEvalOp::evaluate(IndividualP individual)
{
    FitnessP fitness(new FitnessMax);
    auto cartesian = (cartesian::Cartesian*) individual->getGenotype().get();


    std::vector<std::vector<uint>> confusionMatrix(numberOfDifferentClasses, std::vector<uint>(numberOfDifferentClasses,0));
    //std::cout << "Number of diff classes: " << numberOfDifferentClasses << '\n';

//    std::cout << "Jednom prije svih evaluacija.\n";
    for(uint i = 0; i < trainingInput.size(); i++) {
        std::vector<double> results;
        //std::cout << "Prije poziva evaluate.\n";
        cartesian->evaluate(trainingInput[i], results);
        //std::cout << "Nakon poziva evaluate.\n";
        confusionMatrix[utility::vectorArgmax(results)][trainingOutputs[i]]++;
    }
    //std::cout << "Jednom nakon svih evaluacija.\n";
    double valueOfFitness = utility::returnConfusionMatrixResult(confusionMatrix,trainingInput.size(),measureUsed);
    fitness->setValue(valueOfFitness);
//    std::cout << "Povratak iz evaluate.\n";

    return fitness;
}

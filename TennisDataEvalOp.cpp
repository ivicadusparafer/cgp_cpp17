#include "TennisDataEvalOp.h"
#include "./cartesian/Cartesian.h"
#include "utility/fileparser.h"
#include "./utility/measures.h"
#include "./utility/utility.h"
#include <boost/algorithm/string.hpp>
void TennisDataEvalOp::addConstants(uint nConstants)
{
    std::uniform_int_distribution<uint> distribution(0,3);
    for(auto& elem : inputs) {
        for(uint i = 0; i < nConstants; i++) {
            elem.push_back(rng::get_unifrom_random_uint(distribution));
        }
    }
}

void TennisDataEvalOp::registerParameters(StateP stateP)
{
    stateP->getRegistry()->registerEntry("training.infile", (voidP) new (std::string), ECF::STRING);
}

bool TennisDataEvalOp::initialize(StateP stateP)
{
    if(!stateP->getRegistry()->isModified("training.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for training");
        return false;
    }
    voidP vp1 = stateP->getRegistry()->getEntry("training.infile");
    std::string path1 = *((std::string*)vp1.get());
    auto parsedCSV = utility::parseCSVFromFile(path1);
    auto ioPair = utility::parseStringIntoFeaturesAndLabels(parsedCSV,false);
    inputs = std::move(ioPair.first);
    outputs = std::move(ioPair.second);
    std::set<uint> distinct;
    for(const auto& elem : outputs) {
        distinct.insert(elem);
    }
    numberOfDifferentClasses = distinct.size();
    if(!stateP->getRegistry()->isModified("measure")) {
        ECF_LOG(stateP,0,"Default measure of accuracy is used.");
        measureUsed = "accuracy";
    }
    else {
        voidP vpM = stateP->getRegistry()->getEntry("measure");
        measureUsed = *((std::string*)vpM.get());
        boost::algorithm::to_lower(measureUsed);
    }
    return true;
}

FitnessP TennisDataEvalOp::evaluate(IndividualP individual)
{
    FitnessP fitness(new FitnessMax);
    auto cartesian = (cartesian::Cartesian*) individual->getGenotype().get();
    //nInputs = nVariables + nConstants, constants are added to each example in a random fashion
    addConstants(cartesian->nConstants);
    std::vector<std::vector<uint>> confusionMatrix(numberOfDifferentClasses, std::vector<uint>(numberOfDifferentClasses,0));
    for(uint i = 0; i < inputs.size(); i++) {
        std::vector<double> results;
        cartesian->evaluate(inputs[i], results);
        confusionMatrix[utility::vectorArgmax(results)][outputs[i]]++;
    }
    double valueOfFitness = utility::returnConfusionMatrixResult(confusionMatrix,inputs.size(),measureUsed);
    fitness->setValue(valueOfFitness);
    return fitness;
}


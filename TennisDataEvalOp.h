#ifndef CARTESIAN_TENNISDATAEVALOP_H
#define CARTESIAN_TENNISDATAEVALOP_H
#include "../ECF/ECF.h"
#include "../ECF/EvaluateOp.h"
#include <vector>
#include <string>
class TennisDataEvalOp : public EvaluateOp
{
private:
    std::vector<std::vector<double>> inputs;
    std::vector<uint> outputs;
    std::string measureUsed;
    uint numberOfDifferentClasses;
    void addConstants(uint nConstants);
public:
    TennisDataEvalOp()=default;
    void registerParameters(StateP stateP);
    bool initialize(StateP stateP);
    FitnessP evaluate(IndividualP individual);
};
#endif //CARTESIAN_TENNISDATAEVALOP_H

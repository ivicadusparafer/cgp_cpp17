#include "MutConnection.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{
    void MutateConnection::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.connection", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateConnection::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.connection");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateConnection::mutate(GenotypeP gene)
    {
        auto cartesian = (Cartesian*) gene.get();
        const auto& vRef = cartesian->functionSet_->vActiveFunctions_;
        //Connection of which output will be mutated?
        uint whichOutput = rng::get_unifrom_random_uint(0, cartesian->nOutputs - 1);
        auto trail = cartesian->getActiveTrail(whichOutput);
        uint whoIsMutated = trail[rng::get_unifrom_random_uint(0, trail.size() - 1)];
        uint rowNumber = cartesian->getRowNumber(whoIsMutated);
        uint whichConnection = rng::get_unifrom_random_uint(0, vRef[cartesian->operator[](whoIsMutated).value]->getNumOfArgs() - 1);
        uint newConnection = cartesian->randomConnectionGenerator(rowNumber);
        cartesian->operator[](whoIsMutated).inputConnections[whichConnection] = newConnection;
        return true;
    }
}

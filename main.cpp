#include "../ECF/ECF.h"
#include "cartesian/Cartesian.h"
#include "SymbRegEvalOp.h"
//#include "MultipleClassEvalOp.h"
//#include "SymbRegEvalOpTree.h"
int main (int argc, char** argv)  {
    StateP state (new State);
    CartesianP cgp (new cartesian::Cartesian);
    //TreeP treeT (new Tree::Tree);
    state->addGenotype(cgp);
    state->setEvalOp(new SymbRegEvalOp);
    state->initialize(argc,argv);
    state->run();
    return 0;
}
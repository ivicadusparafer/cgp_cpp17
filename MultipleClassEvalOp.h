#ifndef SEMINAR_MULTIPLECLASSEVALOP_H
#define SEMINAR_MULTIPLECLASSEVALOP_H
#include "../ECF/ECF.h"
#include "../ECF/EvaluateOp.h"
#include <string>
#include <vector>
class MultipleClassEvalOp : public EvaluateOp{
private:
    std::vector<std::vector<double>> trainingInput;
    std::vector<uint> trainingOutputs;
    std::vector<std::vector<double>> testingInput;
    std::vector<uint> testingOutputs;
    std::string measureUsed;
    double softTargetBeta;
    double softTargetGamma;
    uint numberOfDifferentClasses;
    bool leadsWithId = false;
    bool softTarget = false;
public:
    MultipleClassEvalOp() = default;
    void registerParameters(StateP stateP);
    bool initialize(StateP stateP);
    FitnessP evaluate(IndividualP individual);
};

#endif //SEMINAR_MULTIPLECLASSEVALOP_H

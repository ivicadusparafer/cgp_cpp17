#ifndef CARTESIAN_SYMBREGEVALOP_H
#define CARTESIAN_SYMBREGEVALOP_H
#include "../ECF/ECF.h"
#include "../ECF/EvaluateOp.h"
#include "utility/expression_evaluation.h"
#include "utility/measures.h"
#include "utility/fileparser.h"
#include <iostream>
#include <fstream>
#include <istream>
#include <sstream>
class SymbRegEvalOp : public EvaluateOp
{
private:
    std::vector<std::vector<double>> inputs;
    std::vector<double> output;
    std::vector<double> kappaOutput;
    std::vector<double> thetaOutput;
    std::string equation;
    double softTargetBeta;
    double softTargetGamma;
    bool softTarget = false;
    void addConstants(uint nConstants);
public:
    SymbRegEvalOp() = default;
    void registerParameters(StateP stateP);
    bool initialize(StateP stateP);
    FitnessP evaluate(IndividualP individual);
};
#endif //CARTESIAN_SYMBREGEVALOP_H
